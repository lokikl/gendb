
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "gendb/version"

Gem::Specification.new do |spec|
  spec.name          = "gendb"
  spec.version       = Gendb::VERSION
  spec.authors       = ["Loki Ng"]
  spec.email         = ["dev@lokikl.com"]

  spec.summary       = %q{Generic DB ORM system with CMS and operations abstraction support.}
  # spec.homepage      = "na"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "no host is allowed now"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency('sinatra', '~> 1')
  spec.add_dependency('sinatra_more', '~> 0.3')
  spec.add_dependency('sinatra-support', '~> 1')
  spec.add_dependency('sinatra-contrib', '~> 1')
  spec.add_dependency('multi_json', '~> 1.0')

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
