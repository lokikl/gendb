require 'sinatra_more/routing_plugin'
require 'sinatra_more/markup_plugin'
require 'sinatra/support'
require 'sinatra/contrib/all'
require 'multi_json'

class Gendb::CMS < Sinatra::Base

  register Sinatra::Contrib

  configure do
    set :root, File.join(File.dirname(__FILE__), 'cms')
    set :raise_errors, Proc.new { false }
    set :show_exceptions, false
    set :views, Proc.new { File.join(root, 'views') }
    set :partial_template_engine, :erb
    set :lock, true
    set :dump_errors, true
    set :logging, true

    mime_type :json, 'application/json'
    mime_type :pdf, 'application/pdf'
    set :erb, :trim => '-'

    also_reload '*.rb'
  end

  before do
    # @request_body = request.body.read
    # @request_at = Time.now
    # Log.inf " => #{env["REQUEST_METHOD"]} #{env["REQUEST_PATH"]}"
  end

  after do
    # process_time = (Time.now - @request_at).round(4)
    # Log.inf "    #{response.status} - #{process_time}s"
  end

  not_found do
    "CMS: Resource not found"
  end

  helpers do
    def replace_pane(i, type, action='list', query={})
      q = @q.dup
      q[i] = [type, action, query]
      q = q.first(i+1)
      "?q=" + Base64.strict_encode64(MultiJson.dump(q, mode: :compat))
    end

    def remove_pane(i)
      q = @q.dup
      q.delete_at(i)
      "?q=" + Base64.strict_encode64(MultiJson.dump(q, mode: :compat))
    end

    def prefix_path(path)
      "#{settings.uri_prefix}/#{path}"
    end

    def site_title
      $site_title ||= (settings.site_title rescue 'GenDB CMS')
    end

    def get_display_options(type)
      @display_options ||= {}
      @display_options[type] ||= MultiJson.load(
        Gendb::DB[:cms_display_options].first(type: type)['options_json']
      ) rescue {}
    end
  end

  get '/?' do
    @schema = Gendb::DB.list
    @q = params[:q] ?
      MultiJson.load(Base64.decode64(params[:q]), mode: :compat) :
      []
    response.headers["Content-Type"] = "text/html"
    erb :"index", :locals => {
    }
  end

  get '/batch-manage-modal/:type/?' do |type|
    @schema = Gendb::DB.list
    erb :"batch-manage-modal", :locals => {
      type: type,
    }
  end

  post '/batch-create/:type/?' do |type|
    options = params[:key].map { |k, vstr|
      vstr = '-' if vstr.to_s == ''
      vstr.split(',').map { |v| {k => v.strip} }
    }
    comb_flattened = if options.length == 1
      options[0]
    else
      combinations = options[0]
      options[1..-1].each { |option| combinations = combinations.product(option) }
      combinations.map { |comb|
        comb.flatten.each_with_object({}) { |hash, h| h.merge!(hash) }
      }
    end

    lock_filter = params[:locks].each_with_object({}) { |(k, v), h| h[k.sub('_uuid', '')] = v }
    lock_attrs = params[:locks]

    db = Gendb::DB[type]
    combination_uuids = comb_flattened.each_with_index.map { |props, i|
      uuids = db.all_uuids(props.merge(lock_filter))
      if uuids.empty?
        db.create(props.merge(lock_attrs).merge(_batch_order: i))
      else
        db.update(uuid: uuids[0], _batch_order: i)
        uuids[0]
      end
    }
    (db.all_uuids(lock_filter) - combination_uuids).each { |uuid|
      db.destroy(uuid: uuid)
    }

    query = (params[:locks] || {}).map { |k, v|
      "locks[#{k}]=#{v}"
    }.join('&')
    redirect prefix_path("/batch-manage-modal/#{type}?#{query}")
  end

  post '/batch-update/:type/?' do |type|
    db = Gendb::DB[type]
    params[:props].each { |uuid, props|
      db.update(props.symbolize_keys.merge(uuid: uuid))
    }
    query = (params[:locks] || {}).map { |k, v|
      "locks[#{k}]=#{v}"
    }.join('&')
    redirect prefix_path("/batch-manage-modal/#{type}?#{query}")
  end

  get '/export/gendb.:timestamp.gendb' do |_timestamp|
    db = Gendb::DB.list.each_with_object({}) { |(table_name, _), h|
      h[table_name] = Gendb::DB[table_name].all
    }
    content_type 'application/octet-stream'
    Zlib::Deflate.deflate(MultiJson.dump(db, mode: :compat))
  end

  post '/flush-all/?' do
    Gendb::DB.list.each { |table_name, _|
      Gendb::DB[table_name].destroy_all
    }
    redirect back
  end

  post '/import/?' do
    json_content = Zlib::Inflate.inflate(params[:file][:tempfile].read)
    records = MultiJson.load(json_content)
    records.each { |table_name, records|
      db = Gendb::DB[table_name]
      all_uuids = db.all_uuids
      records.each { |record|
        unless all_uuids.include?(record['uuid'])
          db.create(record)
        end
      }
    }
    redirect back
  end

  post '/new/?' do
    db = Gendb::DB[params[:type]]
    db.create(params[:props])
    redirect back
  end

  post '/update/?' do
    db = Gendb::DB[params[:type]]
    props = params[:props].symbolize_keys
    removing_props = props.select { |k, v|
      v == '-deleting-after-save-'
    }.map { |k, v| k }
    removing_props.each { |key| props.delete(key) }
    db.update(props.merge(uuid: params[:uuid], _removing_props: removing_props))
    redirect back
  end

  post '/update-display-options/?' do
    db = Gendb::DB[:cms_display_options]
    db.create(
      type: params[:type],
      options_json: MultiJson.dump(params),
    )
    redirect back
  end

  post '/delete/?' do
    db = Gendb::DB[params[:type]]
    db.destroy(uuid: params[:uuid])
    redirect back
  end

  post '/clone/?' do
    definition = Gendb::DB.list[params[:type]]
    db = Gendb::DB[params[:type]]
    record = db.get(params[:uuid])
    %w[uuid created_at updated_at _type].each { |k|
      record.delete(k)
    }
    definition['unique'].each { |rfield|
      record[rfield] = "#{record[rfield]}-#{Time.now.to_i}"
    }
    db.create(record)
    redirect back
  end
end
