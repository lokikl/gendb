require "gendb/version"

require 'yaml'

=begin
  14/3: added flag should_precache_filters for bypassing the re-indexing process
  will greatly speedup the insert and delete operation
  but please remember to recreate_indexes manually

  redisdb.yml:
    test:
      filters: [a, b, c]

  rake p:
    Gendb::DB[:test].all_uuids.length
    100000.times { |i|
      Gendb::DB[:test].create(a: rand(0..10), b: rand(0..10), c: rand(0..10), should_precache_filters: false)
    }
    Gendb::DB[:test].recreate_indexes;nil
    Gendb::DB[:test].destroy_all
=end

module Gendb
  class DB
    # expected keys: [table_name]
    # expected argv: [field1, field2, field3 ...]
    # can debug with: redis.log(redis.LOG_NOTICE, 'message')
    PRECACHE_INDEX_SCRIPT = <<-EOF
      local index_prefix = "index:" .. KEYS[1]

      -- clean up indexes
      for i, field in pairs(ARGV) do
        local index_key = index_prefix .. '=' .. field
        local values = redis.call('smembers', index_key)
        for k, value in pairs(values) do
          redis.call('del', index_key .. ":" .. value)
        end
        redis.call('del', index_key)
      end

      local uuids = redis.call('smembers', index_prefix)
      for i, uuid in pairs(uuids) do
        local values = redis.call('hmget', "resource:" .. uuid, unpack(ARGV))
        for k, field in pairs(ARGV) do
          local index_key = index_prefix .. '=' .. field
          local v = values[k]
          if v == false then v = '' end
          redis.call('sadd', index_key, v)
          redis.call('sadd', index_key .. ":" .. v, uuid)
        end
      end

      return 1
    EOF

    def self.set_redis(redis)
      @@redis = redis
    end

    def self.reload_models(config_yml_paths)
      $_gendb_callbacks ||= {}
      $models = {'models' => {}, 'linkages' => []}
      config_yml_paths.each { |schema_path|
        next unless File.exists?(schema_path)
        schema = YAML.load(File.read(schema_path))
        $models['models'].merge!(schema['models'])
        $models['linkages'] += schema['linkages']
      }
      linkages = $models['linkages']
      $models = $models['models']
      linkages.each { |left, type, right|
        r_type = { '1-n' => 'n-1', 'n-n' => 'n-n', '1-1' => '1-1' }[type]
        raise "Unrecognized type #{type}" if r_type.nil?
        l, r = $models.values_at(left, right)
        raise "Undefined model: #{left}" if l.nil?
        raise "Undefined model: #{right}" if r.nil?
        l['linkages'] ||= {}
        r['linkages'] ||= {}
        l['linkages'][right] = type
        r['linkages'][left] = r_type
      } if linkages

      $models.each { |_, meta| # make unique fields filterable
        filters = meta.fetch('filters', [])
        filters += meta.fetch('unique', [])
        meta['filters'] = filters.uniq
        meta['props'] ||= {}
      }

      # handle ttl
      $models.each { |table, meta|
        if meta.key?('ttl')
          meta['ttl'] = case meta['ttl']
          when 'end-of-day'
            meta['ttl']
          else
            convert_ttl_string_to_seconds(meta['ttl'].to_s)
          end
          if !meta['linkages'].blank?
            raise "TTL and linkages must not co-exists, table: #{table}"
          end
        end
      }
    end

    def convert_end_of_day_to_seconds
      current_time = Time.now
      (current_time.end_of_day - current_time).to_i
    end

    def self.convert_ttl_string_to_seconds(ttl)
      n = ttl[0..-2].to_i
      case ttl[-1]
      when 's'
        n
      when 'm'
        n * 60
      when 'h'
        n * 60 * 60
      when 'd'
        n * 60 * 60 * 24
      when 'w'
        n * 60 * 60 * 24 * 7
      else
        raise "TTL support s, m, h, d, w"
      end
    end

    def self.list
      $models
    end

    def self.[](type)
      @db ||= {}
      @db[type] ||= new(type: type)
    end

    def self.recreate_all_indexes
      list.each { |k, _|
        self[k].recreate_indexes
      }
    end

    def self.deflate(obj)
      obj = obj.to_json unless obj.is_a?(String)
      Base64.encode64(Zlib::Deflate.deflate(obj))
    end

    def self.inflate(string)
      Zlib::Inflate.inflate(Base64.decode64(string))
    end

    def on_update(&callback)
      $_gendb_callbacks[@type] ||= {}
      id = callback.source_location.join(':')
      $_gendb_callbacks[@type][id] = callback
    end

    def recreate_indexes
      precache_filters
      # TODO: uniqueness, required, filter
    end

    private def initialize(type:)
      @type = type.to_s
      @definition = self.class.list[@type]
      @definition['linkages'] ||= {}
      @definition['unique'] ||= {}
      @definition['hidden'] ||= {}
      @definition['required'] ||= {}
      raise "Unknown type #{@type}" if @definition.nil?
    end

    private def redis
      if defined?(@@redis)
        @@redis
      else
        raise "Redis not set, please set with Gendb::DB.set_redis(redis_conn)"
      end
    end

    def gen_random_key
      [*'0'..'9', *'a'..'z', *'A'..'Z'].sample(22).join
    end

    def create(options={})
      options = options.symbolize_keys
      if options[:uuid].nil?
        if @definition.key?('auto_override_by_composite_keys')
          keys = options.slice(*@definition['auto_override_by_composite_keys'].map(&:to_sym))
          options[:uuid] = all_uuids(keys)[0]
        end
        options[:uuid] ||= gen_random_key
      end
      options[:created_at] ||= Time.now.to_i
      options[:_type] = @type
      @definition['required'].each { |key|
        raise "#{key} is required but missing." if options[key.to_sym].to_s == ''
      }
      update(options)
      options[:uuid]
    end


    def update(uuid:, **options)
      options = options.symbolize_keys
      no_callback = options.delete(:_no_callback)
      should_precache_filters = options.delete(:should_precache_filters)
      should_precache_filters = true if should_precache_filters.nil?
      removing_props = options.delete(:_removing_props) || []

      # validations
      options.each { |k, v|
        available_options = @definition.dig('props', k.to_s, 'options')
        if available_options && !available_options.include?(v.to_s)
          raise "#{k} of #{@type} can only be #{available_options.inspect} but got #{v}"
        end
      }

      current = get(uuid)
      if current.nil? && options[:_type] != @type
        raise "Unable to update #{@type}, resource do not exist"
      end

      # uniqueness check
      @definition['unique'].each { |key|
        if options[key.to_sym] && (current || {})[key] != options[key.to_sym] && all_uuids(key => options[key.to_sym]).length > 0
          raise "#{key} has already been taken"
        end
      }

      # handle auto links
      @definition['linkages'].each { |target_type, type|
        if type.end_with?('1') # auto link
          target_uuid = options[:"#{target_type}_uuid"]
          link(uuid: uuid, target_uuid: target_uuid) if target_uuid
        end
      }
      # save to redis
      options[:updated_at] ||= Time.now.to_i
      redis.mapped_hmset("resource:#{uuid}", options.merge(uuid: uuid))
      removing_props.each { |key|
        redis.hdel("resource:#{uuid}", key)
      }
      redis.sadd("index:#{@type}", uuid)

      precache_filters if should_precache_filters

      unless no_callback
        callbacks = $_gendb_callbacks.dig(@type)
        callbacks.each { |_, callback|
          callback.call(uuid)
        } if callbacks
      end

      if @definition['ttl']
        ttl = case @definition['ttl']
        when 'end-of-day'
          convert_end_of_day_to_seconds
        else
          @definition['ttl']
        end
        redis.expire("resource:#{uuid}", ttl)
      end
      true
    end

    private def precache_filters
      return if @definition['filters'].blank?
      sha1 = Digest::SHA1.hexdigest(PRECACHE_INDEX_SCRIPT)
      begin
        redis.evalsha(sha1, [@type], @definition['filters'])
      rescue Redis::CommandError => _
        redis.eval(PRECACHE_INDEX_SCRIPT, [@type], @definition['filters'])
      end
    end

    def link(uuid:, target_uuid:, unlink: false)
      return if uuid.nil? || target_uuid.nil?
      target_type = get_resource_type(uuid: target_uuid)
      return if target_type.nil?
      type = @definition['linkages'][target_type]
      raise "#{@type} has no relationship to #{target_type}" unless type
      unless unlink # auto unlink
        if type[-1] == '1'
          current_target = redis.get("index:#{@type}-#{target_type}:#{uuid}")
          unlink(uuid: uuid, target_uuid: current_target) if current_target
        elsif type[0] == '1'
          current_target = redis.get("index:#{target_type}-#{@type}:#{target_uuid}")
          unlink(uuid: uuid, target_uuid: current_target) if current_target
        end
      end
      methods = unlink ? { 'n' => :srem, '1' => :del } : { 'n' => :sadd, '1' => :set }
      redis.send(methods[type[0]], "index:#{target_type}-#{@type}:#{target_uuid}", uuid)
      redis.send(methods[type[-1]], "index:#{@type}-#{target_type}:#{uuid}", target_uuid)
    end

    def unlink(uuid:, target_uuid:)
      link(uuid: uuid, target_uuid: target_uuid, unlink: true)
    end

    def destroy(uuid:, should_precache_filters: true)
      r_type = get_resource_type(uuid: uuid)
      raise "Destroying resource is not a #{@type}" if @type != r_type
      @definition['linkages'].each { |target_type, type|
        if type.end_with?('n')
          redis.smembers("index:#{@type}-#{target_type}:#{uuid}").each { |target_uuid|
            unlink(uuid: uuid, target_uuid: target_uuid)
          }
          redis.del("index:#{@type}-#{target_type}:#{uuid}")
        else # end with 1
          unlink(
            uuid: uuid,
            target_uuid: redis.hget("resource:#{uuid}", "#{target_type}_uuid"),
          )
        end
      }
      redis.srem("index:#{@type}", uuid)
      redis.del("resource:#{uuid}")
      precache_filters if should_precache_filters
      true
    end

    def destroy_all(options={})
      uuids = all_uuids(options)
      uuids.map { |uuid| destroy(uuid: uuid, should_precache_filters: false) }
      precache_filters
      true
    end

    def all_uuids(options={})
      uuids = options.map { |k, v|
        if %w[n-1 n-n].include?(@definition['linkages'][k.to_s]) # linkages
          redis.smembers("index:#{k}-#{@type}:#{v}")
        elsif @definition['filters'].include?(k.to_s) # filters
          index_key = "index:#{@type}=#{k}"
          redis.sismember(index_key, v) ? redis.smembers("#{index_key}:#{v}") : []
        else
          raise "#{k} cannot be a filter to list #{@type}"
        end
      }.compact
      uuids = [redis.smembers("index:#{@type}")] if uuids.empty?
      op = options[:operation] == :or ? :or : :and
      uuids = op == :or ? uuids.reduce(:|) : uuids.reduce(:&)
      uuids
    end

    def count(options={})
      all_uuids(options).length
    end

    def all(options={})
      fields = options.delete(:_fields)
      uuids = all_uuids(options)
      uuids.map { |uuid| get(uuid, fields: fields) }.compact
    end

    def first(options={})
      fields = options.delete(:_fields)
      uuid = all_uuids(options)[0]
      if uuid
        get(uuid, fields: fields)
      end
    end

    def get(uuid, fields: nil)
      return nil unless redis.exists("resource:#{uuid}")
      hash = if fields.nil?
        redis.hgetall("resource:#{uuid}")
      else
        fields = (fields + %w[uuid _type]).uniq
        Hash[fields.zip(redis.hmget("resource:#{uuid}", fields))]
      end
      raise "Resource is not a #{@type} but a #{hash['_type']}" if hash['_type'] != @type
      @definition['linkages'].each { |target_type, type|
        if type.end_with?('1')
          target_id = redis.get("index:#{@type}-#{target_type}:#{uuid}")
          hash["#{target_type}_uuid"] = target_id
        end
      }
      @definition['hidden'].each { |key| hash.delete(key) }
      hash
    end

    private def get_resource_type(uuid:)
      redis.hget("resource:#{uuid}", "_type")
    end
  end
end

# the builtin CMS
require_relative 'cms'

