$(function() {
  function initRichtextEditor() {
    $('textarea.richtext').each(function() {
      var $editor = $(this)
      $editor.removeClass('richtext')
      $editor.froalaEditor({
        charCounterCount: false,
        height: 300,
        // width: 520,
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough',
          // 'fontFamily', 'fontSize',
          'color',
          // 'inlineStyle', 'paragraphStyle',
          'paragraphFormat', 'align', 'formatOL', 'formatUL',
          // 'outdent', 'indent', 'quote',
          'insertLink',
          // 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll',
          // 'clearFormatting', 'print', 'spellChecker', 'help',
          'html',
          // 'undo', 'redo'
        ]
      })
    });
  }

  $('body').on('statechanged', initRichtextEditor)
  initRichtextEditor()

  $('body').on('click.remove-prop', '[data-remove-prop]', function() {
    var $this = $(this);
    $this.parent().find('input').val('-deleting-after-save-')
  });

  $('body').on('click.toggle', '[data-toggle]', function() {
    var $this = $(this)
    var $target = $($this.data('toggle'))
    $target.toggleClass('hidden')
    return false
  });

  $('body').on('statechanged', function() {
    $('.oneoff-modal').each(function() {
      var $modal = $(this);
      if (!$modal.data('jc')) {
        var jc = $.dialog({
          title: $modal.data('om-title'),
          content: $modal,
          escapeKey: true,
          type: 'red',
          closeIcon: true,
          backgroundDismiss: false,
          columnClass: $modal.data('om-column-class') || 'col-lg-8',
        });
        $modal.remove()
        jc.content.data('jc', jc)
      }
      $modal.removeClass('oneoff-modal')
    });

    $('time.timeago').timeago();
  });

  $('body').trigger('statechanged')
})

