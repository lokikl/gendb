# Gendb

Gemfile
```
gem 'gendb', git: "https://bitbucket.org/lokikl/gendb.git", tag: "0.1.1"
```

config.ru
```
require 'gendb'
Gendb::DB.set_redis(Redis.new)
Gendb::DB.reload_models(['gendb.yml'])

map '/cms' do
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8

  Gendb::CMS.set :uri_prefix, '/cms'
  run Gendb::CMS
end
```

gendb.yml
```
---
models:
  collection:
    required: [name]

  product:
    required: [name, description, price]
    props:
      description:
        type: html
        default: "Sample Description"

linkages:
  - [collection, 1-n, product]
```
