nnoremap sh :exec 'CtrlP lib/cms/views'<cr>
nnoremap sj :exec 'CtrlP lib/cms/public'<cr>
nnoremap sc :exec 'CtrlP lib/cms/public'<cr>

nnoremap <leader>r :call RestartApp()<cr>
" redis-server gendb 52523
function! RestartApp()
  let p = getcwd()
  let cmd = " C-c ' cd " . p . " ; puma -p 8811' Enter"
  call TmuxSendKey("main", "gendb", cmd)
endfunction
