require 'gendb'
require 'redis'
Gendb::DB.set_redis(Redis.new(
  url: 'redis://localhost:52523',
  password: 'redis-password',
))
Gendb::DB.reload_models(['gendb.yml'])
Gendb::DB[:collection].on_update { |uuid|
  Gendb::DB[:collection].update(uuid: uuid, yeah: Time.now.to_i, _no_callback: true)
}

map '/cms' do
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8

  session_conf = {
    secret: "CMSSecrEtIsASeCreTCMS",
    expire_after: 60 * 60 * 24, # 1 day
  }
  use Rack::Session::Cookie, session_conf

  Gendb::CMS.set :uri_prefix, '/cms'
  run Gendb::CMS
end
